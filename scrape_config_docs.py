#! /usr/bin/env python3

import json
import re

from bs4 import BeautifulSoup

def scrape(raw):
    soup = BeautifulSoup(raw, features="lxml")
    return soup

def parse_table(element):
    rows = []
    for row in element.find_all(["tr"]):
        rows.append(tuple(["".join(el.strings) for el in row(["td"])]))
    return [r for r in rows if len(r) != 0]

def format_header(h):
    return re.sub("^[\d\.\s]+", "", h)

def parse(soup):
    headings = ["h3", "h4"]

    data = []
    for element in soup.find_all(headings + ["table"]):
        if element.name in headings:
            name = element.find("a").string
        elif element.name == "table":
            table = parse_table(element)
            data.append({"section": format_header(name), "options": table})
        else:
            print(f"Skipping {element.name} {element.string}")
    return data

def run():
    with open("docs-cleaned.html", "r") as f:
        soup = scrape(f.read())
        data = parse(soup)

        for config in data:
            print(config["section"])

        with open("src/configs.json", "w") as out:
            json.dump(data, out, indent=2)

run()

