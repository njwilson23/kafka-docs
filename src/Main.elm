module Main exposing (..)

-- Load JSON and display it

import Browser
import Dict as Dict exposing (Dict)
import Set as Set exposing (Set)

import Html as Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick)

import Http
import Json.Decode as JD exposing (Decoder, field, string, list)

-- MAIN

main =
  Browser.element
    { init = init
    , update = update
    , subscriptions = subscriptions
    , view = view
    }


-- MODEL

type Model
  = Loading String
  | Failure Http.Error
  | Success (Dict String Config)

type alias Config
  = { options: List Option
    , unfurled: Bool
    }

type alias ConfigData
  = { section: String
    , options: List Option
    }

type alias Option
  = { name: String
    , description: String
    , vtype: String
    , default: String
    }

init : String -> (Model, Cmd Msg)
init jsonPath =
  (Loading jsonPath, getConfigJson jsonPath)


-- UPDATE

type Msg
  = LoadConfig String
  | GotConfig (Result Http.Error (Dict String Config))
  | Unfurl String
  | Furl String

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case (msg, model) of
    (LoadConfig path, _) ->
      (Loading path, getConfigJson path)

    (GotConfig result, _) ->
      case result of
        Ok configs ->
          (Success configs, Cmd.none)

        Err error ->
          (Failure error, Cmd.none)

    (Unfurl section, Success configs) ->
      (Success (Dict.update section (Maybe.map (\prev -> { prev | unfurled = True })) configs), Cmd.none)

    (Furl section, Success configs) ->
      (Success (Dict.update section (Maybe.map (\prev -> { prev | unfurled = False })) configs), Cmd.none)

    -- The following two states don't make any sense, and if they ever happen,
    -- it's best to reload from scratch

    (Unfurl section, previous) ->
      (previous, getConfigJson "")

    (Furl section, previous) ->
      (previous, getConfigJson "")


-- SUBSCRIPTIONS

subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none


-- VIEW

view : Model -> Html Msg
view model =
  Html.div []
    [ Html.h2 [] [ Html.text "Kafka Configs" ]
    , viewConfigs model
    ]


viewConfigs : Model -> Html Msg
viewConfigs model =
  case model of
    Loading _ ->
      text "Loading..."

    Failure error ->
      Html.div []
        [ Html.text (errorToString error)
        ]

    Success configOptions ->
      Html.div []
        [ Html.div [] (Dict.values (Dict.map viewOneConfig configOptions))
        ]


viewOneConfig : String -> Config -> Html Msg
viewOneConfig section config =
  case config.unfurled of
    True ->
      Html.div
        [ Html.Attributes.class "config" ]
        [ viewSection section config.unfurled
        , viewOptions config.options
        ]

    False ->
      Html.div
        [ Html.Attributes.class "config" ]
        [ viewSection section config.unfurled ]


viewSection : String -> Bool -> Html Msg
viewSection section unfurled =
  let
    cursorPointerStyle =
      Html.Attributes.style "cursor" "pointer"
  in
  case unfurled of
    True ->
      Html.div [ onClick (Furl section) ]
        [ Html.h3 [ cursorPointerStyle ] [Html.text ("[-] " ++ section) ] ]

    False ->
      Html.div [ onClick (Unfurl section) ]
        [ Html.h3 [ cursorPointerStyle ] [ Html.text ("[+] " ++ section) ] ]


viewOptions : List Option -> Html Msg
viewOptions options =
  Html.div []
    (List.map (viewOneOption (allOptionNames options)) options)


-- We also pass a set of the other options that exist so that we can create links
viewOneOption : Set String -> Option -> Html Msg
viewOneOption otherOptions option =
  let
    optClass =
      Html.Attributes.class "option"
    optNameClass =
      Html.Attributes.class "option-name"
    optTypeClass =
      Html.Attributes.class "option-type"
    optDefaultClass =
      Html.Attributes.class "option-default"
    optRowTop =
      Html.Attributes.class "option-row-top"
  in
  Html.div [] [ Html.div [ optRowTop ]
                         [ Html.div [ optClass, optNameClass ] [ localAnchor option.name [ Html.text option.name ] ]
                         , Html.div [ optClass, optTypeClass ] [ Html.text option.vtype ]
                         , Html.div [ optClass, optDefaultClass ] [ Html.text option.default ]
                         ]
              , viewDescription otherOptions option.description
              , Html.hr [] []
              ]


viewDescription : Set String -> String -> Html Msg
viewDescription otherOptions content =
  Html.div [ Html.Attributes.class "option-description" ] (linkify otherOptions content)


-- Replace parts of the input that look like option names with links
-- find likely candidates with a regex
-- for each candidate, check the list of options, and either insert a link or the original text
linkify : Set String -> String -> List (Html Msg)
linkify otherOptions content =
  List.map (asLinkOrText otherOptions) (String.split " " content)


asLinkOrText : Set String -> String -> Html Msg
asLinkOrText whitelist s =
  if Set.member s whitelist then
    Html.a [ Html.Attributes.href ("#" ++ s) ] [ Html.text (s ++ " ") ]
  else
    Html.text (s ++ " ")


-- Create an anchor tag that links to itself
localAnchor : String -> List (Html Msg) -> Html Msg
localAnchor name htmls =
  Html.a [ Html.Attributes.id name, Html.Attributes.href ("#" ++ name) ] htmls


-- Get a set of all option names
allOptionNames : List Option -> Set String
allOptionNames options =
  Set.fromList ( List.map (\opt -> opt.name) options)


errorToString : Http.Error -> String
errorToString error =
    case error of
        Http.BadUrl url ->
            "The URL " ++ url ++ " was invalid"
        Http.Timeout ->
            "Unable to reach the server, try again"
        Http.NetworkError ->
            "Unable to reach the server, check your network connection"
        Http.BadStatus 500 ->
            "The server had a problem, try again later"
        Http.BadStatus 400 ->
            "Verify your information and try again"
        Http.BadStatus _ ->
            "Unknown error"
        Http.BadBody errorMessage ->
            errorMessage


-- HTTP

getConfigJson : String -> Cmd Msg
getConfigJson path =
  Http.get
    { url = path
    , expect = Http.expectJson GotConfig configDictDecoder
    }

optionDecoder : JD.Decoder Option
optionDecoder =
  JD.map4
    Option
    (JD.index 0 JD.string)
    (JD.index 1 (JD.oneOf [JD.string, JD.null ""]))
    (JD.index 2 (JD.oneOf [JD.string, JD.null ""]))
    (JD.index 3 (JD.oneOf [JD.string, JD.null ""]))

optionListDecoder : JD.Decoder (List Option)
optionListDecoder =
  JD.list optionDecoder

configDecoder : JD.Decoder ConfigData
configDecoder =
  JD.map2 ConfigData (JD.field "section" JD.string) (JD.field "options" optionListDecoder)

configDictDecoder : JD.Decoder (Dict String Config)
configDictDecoder =
  JD.map
    (\datas ->
      Dict.fromList
        (List.map (\data -> (data.section, { options = data.options, unfurled = False })) datas))
    (JD.list configDecoder)
