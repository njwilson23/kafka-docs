package com.mapfold.kafkadocs

import io.circe
import io.circe.*
import io.circe.Encoder
import io.circe.generic.semiauto.*
import io.circe.syntax.*
import kafka.metrics.KafkaMetricsConfig
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.config.ConfigDef
import org.apache.kafka.common.config.internals.BrokerSecurityConfigs
import org.apache.kafka.connect.runtime.distributed.{Crypto, DistributedConfig}
import org.apache.kafka.connect.runtime.standalone.StandaloneConfig
import org.apache.kafka.coordinator.group.GroupCoordinatorConfig
import org.apache.kafka.coordinator.transaction.{TransactionLogConfigs, TransactionStateManagerConfigs}
import org.apache.kafka.network.SocketServerConfigs
import org.apache.kafka.security.PasswordEncoderConfigs
import org.apache.kafka.server.config.{DelegationTokenManagerConfigs, KRaftConfigs, QuotaConfigs, ReplicationConfigs, ServerConfigs, ServerLogConfigs, ZkConfigs}
import org.apache.kafka.server.log.remote.metadata.storage.TopicBasedRemoteLogMetadataManagerConfig
import org.apache.kafka.server.metrics.MetricConfigs
import org.apache.kafka.storage.internals.log.{CleanerConfig, LogConfig}
import org.apache.kafka.streams.StreamsConfig

import java.io.{File, FileWriter, InvalidClassException}
import java.lang.reflect.Field
import scala.jdk.CollectionConverters.*
import scala.reflect.ClassTag

enum Type {
  case Boolean, String, Int, Short, Long, Double, List, Class, Password
}

object Type {

  def fromConfigKeyType(t: ConfigDef.Type): Type =
    t match {
      case ConfigDef.Type.BOOLEAN => Boolean
      case ConfigDef.Type.STRING => String
      case ConfigDef.Type.INT => Int
      case ConfigDef.Type.SHORT => Short
      case ConfigDef.Type.LONG => Long
      case ConfigDef.Type.DOUBLE => Double
      case ConfigDef.Type.LIST => List
      case ConfigDef.Type.CLASS => Class
      case ConfigDef.Type.PASSWORD => Password
    }

  implicit val enc: Encoder[Type] = new Encoder[Type]:
    override def apply(a: Type): Json = a match {
      case Boolean => Json.fromString("boolean")
      case String => Json.fromString("string")
      case Int => Json.fromString("int")
      case Short => Json.fromString("short")
      case Long => Json.fromString("long")
      case Double => Json.fromString("double")
      case List => Json.fromString("list")
      case Class => Json.fromString("class")
      case Password => Json.fromString("password")
    }
}

case class Config(name: String, doc: String, datatype: Type, default: Option[AnyRef], valid: String)

object Config {
  implicit val enc: Encoder[Config] = deriveEncoder

  implicit private def encAnyRef: Encoder[AnyRef] = new Encoder[AnyRef]:
    override def apply(a: AnyRef): Json = Option(a).map(_.toString).getOrElse("").asJson
}

object Configs {

  private def hasField[C: ClassTag](name: String): Boolean =
    implicitly[ClassTag[C]].runtimeClass.getDeclaredFields.exists(_.getName == name)

  private def getPrivateField[C: ClassTag, V](name: String): V = {
    val field = implicitly[ClassTag[C]].runtimeClass.getDeclaredField(name)
    field.setAccessible(true)
    field.get(null).asInstanceOf[V]
  }

  def fromClass[C: ClassTag]: Seq[Config] =
    if (hasField[C]("CONFIG")) fromConfigDef(getPrivateField[C, ConfigDef]("CONFIG"))
    else if (hasField[C]("CONFIG_DEF")) fromConfigDef(getPrivateField[C, ConfigDef]("CONFIG_DEF"))
    else throw new InvalidClassException(implicitly[ClassTag[C]].runtimeClass.getName, "does not have a CONFIG or CONFIG_DEF member")

  def fromClassWithName[C: ClassTag](name: String): Seq[Config] =
    fromConfigDef(getPrivateField[C, ConfigDef](name))

  def fromConfigDef(configDef: ConfigDef): Seq[Config] =
    configDef.configKeys().asScala.map { case (_, key) =>
      val validStr = Option(key.validator).map(_.toString).getOrElse("")
      Config(key.name, key.documentation, Type.fromConfigKeyType(key.`type`), Some(key.defaultValue), validStr)
    }.toSeq
}

case class Section(name: String, configs: Seq[Config])

object Section {
  implicit val enc: Encoder[Section] = deriveEncoder
}

object Main {

  def main(args: Array[String]): Unit = {

    // Server config
    val server =
        Configs.fromClass[ServerConfigs] ++
        Configs.fromClass[ZkConfigs] ++
        Configs.fromClass[SocketServerConfigs] ++
        Configs.fromClass[BrokerSecurityConfigs] ++
        Configs.fromClass[QuotaConfigs] ++
        Configs.fromClass[PasswordEncoderConfigs] ++
        Configs.fromClassWithName[GroupCoordinatorConfig]("GROUP_COORDINATOR_CONFIG_DEF") ++
        Configs.fromClassWithName[GroupCoordinatorConfig]("NEW_GROUP_CONFIG_DEF") ++
        Configs.fromClassWithName[GroupCoordinatorConfig]("OFFSET_MANAGEMENT_CONFIG_DEF") ++
        Configs.fromClassWithName[GroupCoordinatorConfig]("CONSUMER_GROUP_CONFIG_DEF") ++
        Configs.fromClassWithName[GroupCoordinatorConfig]("SHARE_GROUP_CONFIG_DEF") ++
        Configs.fromClass[KRaftConfigs] ++
        Configs.fromClass[TopicBasedRemoteLogMetadataManagerConfig] ++
        Configs.fromClass[LogConfig] ++
        Configs.fromClassWithName[LogConfig]("SERVER_CONFIG_DEF") ++
        Configs.fromClass[ReplicationConfigs] ++
        Configs.fromClass[CleanerConfig] ++
        Configs.fromClass[TransactionLogConfigs] ++
        Configs.fromClass[TransactionStateManagerConfigs] ++
        Configs.fromClass[DelegationTokenManagerConfigs] ++
        Configs.fromClass[MetricConfigs]

    // Topic config
    val log = Configs.fromClass[LogConfig]

    // Producer config
    val producer = Configs.fromClass[ProducerConfig]

    // Consumer config
    val consumer = Configs.fromClass[ConsumerConfig]

    // Admin config
    val admin = Configs.fromClass[AdminClientConfig]

    // Connect config
    val connectStandalone = Configs.fromClass[StandaloneConfig]
    val connectDistributed = {
      val field = implicitly[ClassTag[DistributedConfig]].runtimeClass.getDeclaredMethod("config", classOf[Crypto])
      field.setAccessible(true)
      val configDef = field.invoke(null, Crypto.SYSTEM).asInstanceOf[ConfigDef]
      Configs.fromConfigDef(configDef)
    }

    // Streams config
    val streams = Configs.fromClass[StreamsConfig]

    val allConfig =
        Section("Server", server) ::
        Section("Topic-level", log) ::
        Section("Producer Client", producer) ::
        Section("Consumer Client", consumer) ::
        Section("Admin Client", admin) ::
        Section("Connect", connectStandalone ++ connectDistributed) ::
        Section("Streams", streams) ::
        Nil

    println(allConfig.asJson)
    val fileWriter = new FileWriter(new File("configs.json"))
    try {
      fileWriter.write(allConfig.asJson.toString)
    } finally {
      fileWriter.close()
    }
  }
}
