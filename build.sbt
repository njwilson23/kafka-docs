ThisBuild / scalaVersion := "3.5.2"

libraryDependencies += ("org.apache.kafka" %% "kafka" % "3.9.0") cross CrossVersion.for3Use2_13
libraryDependencies += "org.apache.kafka" % "connect-runtime" % "3.9.0"
libraryDependencies += "org.apache.kafka" % "kafka-streams" % "3.9.0"

libraryDependencies += "io.circe" %% "circe-core" % "0.14.10"
libraryDependencies += "io.circe" %% "circe-generic" % "0.14.10"

